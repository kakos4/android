package com.qzpg.quizprog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btntrue, btnfalse, mNextButton, mBackButton;
    private TextView mQuestionTextView;
    private Questions[] mQuestionBank = new Questions[] {
            new Questions(R.string.question_kharkov, true),
            new Questions(R.string.question_oceans, false),
            new Questions(R.string.question_atlantida, true),
            new Questions(R.string.question_africa, true),
            new Questions(R.string.question_americas, true),
    };
    private int mCurrentIndex = 0;
    private static final String TAG = "MainActivity";
    private static final String KEY_INDEX = "index";
    private int mark = 0;
    private int percentMark = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate(Bundle) called");
        onClickBtn();
        updateQuestion();

        if (savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        Log.d(TAG, "onStart() called");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, "onResume() called");
    }

    @Override
    public void onPause(){
        super.onPause();
        Log.d(TAG, "onPause() called");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
    }

    @Override
    public void onStop(){
        super.onStop();
        Log.d(TAG, "onStop() called");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "onDestroy() called");
    }

    public void onClickBtn(){
        btntrue = (Button)findViewById(R.id.btn_true);
        btntrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
                btnClickable(false);
            }
        });

        btnfalse = (Button)findViewById(R.id.btn_false);
        btnfalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
                btnClickable(false);
            }
        });


        mNextButton = (Button)findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentIndex == mQuestionBank.length - 1) {
                    percentMark = 100 / (mQuestionBank.length) * mark;
                    Toast newToast = Toast.makeText(MainActivity.this, "Это последний вопрос! " + "Ваша оценка: " + percentMark + "%", Toast.LENGTH_SHORT);
                    newToast.setGravity(Gravity.TOP, 0, 0);
                    newToast.show();
                } else if (mCurrentIndex != 0 || mCurrentIndex == 0){
                    mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                    updateQuestion();
                    btnClickable(true);
                }
            }
        });

        mBackButton = (Button)findViewById(R.id.back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentIndex != 0) {
                    mCurrentIndex = (mCurrentIndex - 1) % mQuestionBank.length;
                    updateQuestion();
                } else if(mCurrentIndex == 0){
                    Toast newToast = Toast.makeText(MainActivity.this, "Это первый вопрос!", Toast.LENGTH_SHORT);
                    newToast.setGravity(Gravity.TOP, 0, 0);
                    newToast.show();
                }
            }
        });

        mQuestionTextView = (TextView)findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
                updateQuestion();
            }
        });
    }
    public void updateQuestion(){
        int question = mQuestionBank[mCurrentIndex].getTextResId();
        mQuestionTextView.setText(question);
    }

    public void checkAnswer(boolean userPressedTrue) {
        boolean answerIsTrue = mQuestionBank[mCurrentIndex].isAnswerTrue();
        int messageResId = 0;
        if (userPressedTrue == answerIsTrue) {
            messageResId = R.string.toast_correct;
            mark += 1;
        } else {
            messageResId = R.string.toast_incorrect;
        }
        Toast newToast = Toast.makeText(MainActivity.this, messageResId, Toast.LENGTH_SHORT);
        newToast.setGravity(Gravity.TOP, 0, 0);
        newToast.show();
    }

    public void btnClickable(boolean p){
        if(p == false){
            btntrue.setEnabled(false);
            btnfalse.setEnabled(false);
        }else{
            btntrue.setEnabled(true);
            btnfalse.setEnabled(true);
        }
    }
}
